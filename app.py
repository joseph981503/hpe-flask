import pickle as pickle
from annoy import AnnoyIndex
from flask import Flask
from flask import request
from flask import jsonify
import os

app = Flask(__name__)

u = AnnoyIndex(64)
u.load('./kkbox_openapi.ann')

convert_dict = pickle.load(open('./id_to_index.pkl', 'rb'), encoding='iso-8859-1')

def load_emb(path):
    detail_mapping = dict()
    with open(path) as fin:
        for line in fin:
            id_, *value = line.strip().split(' ')
            vector = list(map(float, value))
            detail_mapping[id_] = vector
    return detail_mapping


detail_emb = load_emb('./openapiHpe.txt')

@app.route('/query')
def query():
    
    rtn = []
    i_id = 'i-' + request.args.get('song_id')

    if (i_id in detail_emb):
        print("Query song_id:" + i_id)
        result = u.get_nns_by_vector(detail_emb[i_id], 20, search_k=-1, include_distances=True)
        print(result[0])
        for key in result[0]:
            rtn.append(list(convert_dict.keys())[list(convert_dict.values()).index(key)])
        
        rtn = jsonify(rtn)

    return rtn

if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
